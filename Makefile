NAME=namac

CC=cc
CFILES=namac.c
LIBS=-lgc

all:
	$(CC) -o $(NAME) $(CFILES) $(LIBS)

clean:
	rm -f $(NAME)
