/*
     * Copyright © 2019-2020, a nanon from nanochan, see this thread: http://nanochanpwz3xnue76gteysv6wjm5sim3bbcm2lc65x625at774k77qd.onion/g/6993.html
     * This program is free software. It comes without any warranty, to
     * the extent permitted by applicable law. You can redistribute it
     * and/or modify it under the terms of the Do What The Fuck You Want
     * To Public License, Version 2, as published by Sam Hocevar. See
     * http://www.wtfpl.net/ for more details.
*/
#include<stdio.h>
#include<stdbool.h>
#include<stdlib.h>
#include<string.h>
#include<gc/gc.h>
#include<sys/stat.h>
#include<unistd.h>
#define GENERAL_SIZE_LIMIT 2886
#define HEADER_BEGIN_HTML5 "<!DOCTYPE html>\n<html lang=\"en\">\n        <head>\n                <meta charset=\"UTF-8\">\n"
#define HEADER_BEGIN_XHTML "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">\n        <head>\n                <meta http-equiv=\"Content-type\" content=\"application/xhtml+xml;charset=utf-8\"/>\n"
//#define HEADER_BEGIN_XHTML "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n		<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd\" xml:lang=\"en\">\n        <head>\n                <meta http-equiv=\"Content-type\" content=\"application/xhtml+xml;charset=utf-8\"/>\n"
#define TITLE_BEGIN "                <title>"
#define BEGIN_STYLE_SHEET "                <link rel=\"stylesheet\" href=\""
#define STYLE_SHEET_FINISH "\"/>" //Doubles as finisher for <img>
#define TITLE_END "</title>\n"
#define HEADER_END "        </head>\n"
#define BODY_BEGIN "        <body>"
#define BODY_END "        </body>\n</html>"
#define a_BEGIN "<a href=\""
#define a_MIDDLE "\">" 
#define a_END "</a>"
//Takes a 2d string (character array) and alphabetizes it
int alphabetize(char str[GENERAL_SIZE_LIMIT][GENERAL_SIZE_LIMIT],int i) {
	int j;
	int k = 0;
	for(j = 0;j < i;j++) {
		char max[GENERAL_SIZE_LIMIT];
		strcpy(max,str[j]);
		for(k = j;k < i;k++) {
			if(strcmp(str[k],max) < 0) {
				char tmp[GENERAL_SIZE_LIMIT];
				strcpy(tmp,str[k]);
				strcpy(str[k],max);
				strcpy(max,tmp);
			}
		}
		strcpy(str[j],max);
	}
	return 0;
}
//Below: Function that's supposed to be in string.h, but isn't.
char *strrev(char *str) {
	char *p1, *p2;
	if(! str || ! *str) {
            return str;
	}
	for(p1 = str, p2 = str + strlen(str) - 1; p2 > p1; ++p1, --p2) {
		*p1 ^= *p2;
		*p2 ^= *p1;
		*p1 ^= *p2;
	}
	return str;
}
char *key_src_num[GENERAL_SIZE_LIMIT][GENERAL_SIZE_LIMIT] = {NULL};
int sources = 0;
char src_buffer6[7000];
char *sources_handler(char *keyword) {
	FILE *fp3;
	long fsize3 = 0;
	int i_found_prev = 1;
	char *fvar3;
	char *lnk_to_src = GC_malloc(100);
	char *src_buffer = '\0';
	char *tok_pntr;
	char *str_pntr;
	char src_buffer2[GENERAL_SIZE_LIMIT] = "\0";
	char src_buffer3[GENERAL_SIZE_LIMIT] = "\0";
	char src_buffer4[GENERAL_SIZE_LIMIT] = "\0";
	char src_buffer5[GENERAL_SIZE_LIMIT] = "\0";
	char sources_str[10];
	bool found = false;
	bool found_prev = false;
	//printf("key_src_num[%d][0] = %s\nkey_src_num[%d][1] = %s\n",sources,key_src_num[sources][0],sources,key_src_num[sources][1]);
	while(found_prev == false && i_found_prev <= sources) {
		if(!strcmp(key_src_num[i_found_prev][0],keyword)) {
			strcpy(lnk_to_src,key_src_num[i_found_prev][1]);
			found_prev = true;
			
		}
		i_found_prev++;
	}
	if(found_prev == false) {
		if((fp3 = fopen("webography","r"))) {
			if(fp3 != NULL) {
			fseek(fp3,0,SEEK_END);
				if((fsize3 = ftell(fp3))) {
					fseek(fp3,0,SEEK_SET);
					fvar3 = GC_malloc(fsize3++);
					fread(fvar3,1,fsize3,fp3);
					src_buffer = strtok_r(fvar3,"\n",&tok_pntr);
					while(src_buffer != NULL && found == false) {
						//printf("src_buffer = %s\n",src_buffer);
						if(strstr(src_buffer,keyword)) {
							sources++;
							sprintf(sources_str,"%d",sources);
							strcpy(lnk_to_src,"<sup><a href=\"#s");
							strcat(lnk_to_src,sources_str);
							strcat(lnk_to_src,"\">[");
							strcat(lnk_to_src,sources_str);
							strcat(lnk_to_src,"]</a></sup>");
							found = true;
							key_src_num[sources][0] = GC_malloc(GENERAL_SIZE_LIMIT);
							strcpy(key_src_num[sources][0],keyword);
							key_src_num[sources][1] = GC_malloc(GENERAL_SIZE_LIMIT);
							strcpy(key_src_num[sources][1],lnk_to_src);
						} else {
							src_buffer = strtok_r(NULL,"\n",&tok_pntr);
						}
					}
					src_buffer = strtok_r(NULL,"\n",&tok_pntr);
					while(strstr(src_buffer,"L: ") && src_buffer != NULL) {
						strcpy(src_buffer2,src_buffer);
						strrev(src_buffer2);
						str_pntr = strstr(src_buffer2," :L");
						strcpy(str_pntr,"\0");
						strrev(src_buffer2);
						strcat(src_buffer3," <a href=\"");
						strcat(src_buffer3,src_buffer2);
						strcat(src_buffer3,"\">");
						strcat(src_buffer3,src_buffer2);
						strcat(src_buffer3,"</a>");
						src_buffer = strtok_r(NULL,"\n",&tok_pntr);
					}
					if(strstr(src_buffer,"N: ")) {
						strcpy(src_buffer4,src_buffer);
						strrev(src_buffer4);
						str_pntr = strstr(src_buffer4," :N");
						strcpy(str_pntr,"\0");
						strrev(src_buffer4);
						strcat(src_buffer4,", ");
						src_buffer = strtok_r(NULL,"\n",&tok_pntr);
					} else {
						printf("ERROR: Didn't find \"N: \" for \"%s\", exiting.",keyword);
						return 0;
					}
					if(strstr(src_buffer,"D: ")) {
						strcpy(src_buffer5,src_buffer);
						strrev(src_buffer5);
						str_pntr = strstr(src_buffer5," :D");
						strcpy(str_pntr,"\0");
						strrev(src_buffer5);
					} else {
						printf("ERROR: Didn't find \"D: \" for \"%s\", exiting.",keyword);
						return 0;
					}
					fclose(fp3);
					strcat(src_buffer6,"					        <li id=\"s");
					strcat(src_buffer6,sources_str);
					/*strcat(src_buffer6,"\" value=\"");
					strcat(src_buffer6,sources_str);*/
					strcat(src_buffer6,"\">");
					strcat(src_buffer6,sources_str);
					strcat(src_buffer6,". ");
					strcat(src_buffer6,src_buffer4);
					strcat(src_buffer6,src_buffer5);
					strcat(src_buffer6,src_buffer3);
					strcat(src_buffer6,"</li>\n");
					//printf("src_buffer6 = %s\n",src_buffer6);
				}
			}
		}
	}
	return lnk_to_src;
}
/*
	* Below: Function for deleting an arbitrary string within a string (substring)
	* haystack = the string you want to delete from (ie "lorem ipsum")
	* needle = the (sub)string you want to delete from the haystack (ie "ipsum")
	* the haystack after running rmsubstr("lorem ipsum","ipsum"); would be "lorem"
*/
char *rmsubstr(char *haystack,const char *needle) { //const haystack maybe?
	const int needle_length = strlen(needle);
	char *rmsubstr_buffer = GC_malloc(GENERAL_SIZE_LIMIT);
	strcpy(rmsubstr_buffer,haystack);
	while((haystack = strstr(rmsubstr_buffer,needle))) {
		memmove(haystack,haystack+needle_length,1+strlen(haystack+needle_length));
	}
	return rmsubstr_buffer;
}
char *single_style_handler(int argc,char *argv[]) {
    int i_styles = 0;
    char *outgoing = GC_malloc(GENERAL_SIZE_LIMIT);
    while(i_styles < argc) {
        if(strstr(argv[i_styles],".css")) {
            strcat(outgoing,BEGIN_STYLE_SHEET);
            strcat(outgoing,argv[i_styles]);
            strcat(outgoing,STYLE_SHEET_FINISH);
            strcat(outgoing,"\n");
            break;
        }
        i_styles++;
    }
    return outgoing;
}
char directory_name[GENERAL_SIZE_LIMIT];
bool fmsh_fistr = true;
void multi_style_handler(int argc,char *argv[],char incoming1[],char incoming2[],char incoming3[],char filename_dot_html[]) {
    directory_name[0] = '\0';
    char outgoing2[GENERAL_SIZE_LIMIT] = "\0";
    char *str_pntr;
    int i_styles = 0;
    FILE *fp1;
    while(i_styles < argc) {
        if(strstr(argv[i_styles],".css")) {
            strcat(outgoing2,BEGIN_STYLE_SHEET);
            strcat(outgoing2,argv[i_styles]);
            strcat(outgoing2,a_MIDDLE);
            strcat(outgoing2,"\n");
            if(strstr(argv[i_styles],"/")) {
                strcpy(directory_name,argv[i_styles]);
                strrev(directory_name);
                str_pntr = strstr(directory_name,"/");
                strcpy(str_pntr,"\0");
                strrev(directory_name);
            }
            str_pntr = strstr(directory_name,".css");
            strcpy(str_pntr,"\0");
            if((i_styles >= 4) && (fmsh_fistr == true)) {
                mkdir(directory_name,0700);
                chdir(directory_name);
				fp1 = fopen(filename_dot_html,"w");
                fprintf(fp1,"%s%s%s%s",incoming1,outgoing2,incoming2,incoming3);
				fclose(fp1);
                chdir("..");
			}
            if((i_styles >= 4)) {
                chdir(directory_name);
				fp1 = fopen(filename_dot_html,"a");
                fprintf(fp1,"%s%s",incoming2,incoming3);
				fclose(fp1);
                chdir("..");
			}
        }
        i_styles++;
        outgoing2[0] = '\0';
    }
	fmsh_fistr = false;
}
char th_buffer3[GENERAL_SIZE_LIMIT];
void template_handler(char *filename_extension,char *filename_without_extension) {
	FILE *th_fp;
	FILE *th_fp2;
	FILE *th_fp3;
	FILE *th_fp4;
	char *th_fvar;
	char *th_fvar3;
	char *th_fvar4;
	char *th_fvar_buffer;
	char *th_fvar_buffer3;
	char *th_fvar_buffer4;
	char *th_tok_pntr;
	char *th_tok_pntr2;
	char *th_tok_pntr4;
	char *str_pntr;
	char th_buffer[GENERAL_SIZE_LIMIT];
	char th_buffer2[GENERAL_SIZE_LIMIT];
	char title[GENERAL_SIZE_LIMIT];
	strcpy(th_buffer3,"\0");
	char th_buffer4[GENERAL_SIZE_LIMIT];
	strcpy(th_buffer4,filename_without_extension);
	strcat(th_buffer4,".nama");
	long th_fsize = 0;
	long th_fsize3 = 0;
	long th_fsize4 = 0;
	int th_i = 0;
	bool continue_writing = true;
	strcpy(th_buffer,"booger");
	strcat(th_buffer,filename_extension);
	if((th_fp = fopen("template","r")) && (th_fp2 = fopen(th_buffer,"w")) && (th_fp3 = fopen("directory","r")) && (th_fp4 = fopen(th_buffer4,"r"))) {
		if(th_fp != NULL && th_fp2 != NULL && th_fp3 != NULL && th_fp4 != NULL) {
			fseek(th_fp,0,SEEK_END);
			fseek(th_fp3,0,SEEK_END);
			fseek(th_fp4,0,SEEK_END);
			if((th_fsize = ftell(th_fp)) && (th_fsize3 = ftell(th_fp3)) && (th_fsize4 = ftell(th_fp4))) {
				fseek(th_fp,0,SEEK_SET);
				fseek(th_fp3,0,SEEK_SET);
				fseek(th_fp4,0,SEEK_SET);
				th_fvar = GC_malloc(th_fsize++);
				th_fvar3 = GC_malloc(th_fsize3++);
				th_fvar4 = GC_malloc(th_fsize4++);
				fread(th_fvar,1,th_fsize,th_fp);
				fread(th_fvar3,1,th_fsize3,th_fp3);
				fread(th_fvar4,1,th_fsize4,th_fp4);
				th_fvar_buffer = strtok_r(th_fvar,"\n",&th_tok_pntr2);
				th_fvar_buffer3 = strtok_r(th_fvar3,"\n",&th_tok_pntr);
				th_fvar_buffer4 = strtok_r(th_fvar4,"\n",&th_tok_pntr4);
				while(th_fvar_buffer4 != NULL) {
					if((str_pntr = strstr(th_fvar_buffer4,"title:"))) {
						strcpy(title,TITLE_BEGIN);
						strcat(title,str_pntr+7);
						strcat(title,"</title>");
						fclose(th_fp4);
						break;
					}
					th_fvar_buffer4 = strtok_r(NULL,"\n",&th_tok_pntr4);
				}
				while(th_fvar_buffer != NULL) {
					if(strstr(th_fvar_buffer,"{title}")) {
						fprintf(th_fp2,"\n%s",title);
					}
					if(strstr(th_fvar_buffer,"{directory}")) {
						while(th_fvar_buffer3 != NULL) {
							fprintf(th_fp2,"\n				%s%s%s%s%s%s\n				<br/>",a_BEGIN,th_fvar_buffer3,filename_extension,a_MIDDLE,th_fvar_buffer3,a_END);
							th_fvar_buffer3 = strtok_r(NULL,"\n",&th_tok_pntr);
						}
						//printf("th_fvar_buffer3 = %s\n",th_fvar_buffer3);
					} else if(strstr(th_fvar_buffer,"{content}")) {
						continue_writing = false;
						/*fclose(th_fp);
						fclose(th_fp2);
						fclose(th_fp3);*/
						strcpy(th_buffer2,filename_without_extension);
						strcat(th_buffer2,filename_extension);
						rename(th_buffer,th_buffer2);
					} else if(continue_writing == true && (!strstr(th_fvar_buffer,"{title}"))) {
						if(th_i > 0) {
							fprintf(th_fp2,"\n");
						}
						fprintf(th_fp2,"%s",th_fvar_buffer);
					} else if(continue_writing == false) {
						strcat(th_buffer3,"\n");
						strcat(th_buffer3,th_fvar_buffer);
					}
					th_fvar_buffer = strtok_r(NULL,"\n",&th_tok_pntr2);
					th_i++;
				}
			}
		}
		fclose(th_fp);
		fclose(th_fp2);
		fclose(th_fp3);
	} else {
		printf("You either don't have a template, don't have permission, or don't have a directory file.  Exiting.\n");
		exit(1);
	}
}
char *rm_leading_whitespace(const char in[]) {
	char *out = GC_malloc(strlen(in));
	strcpy(out,in);
	int i2 = 0;
	char *str_pntr;
	strrev(out);
	int i_whitespace = strlen(out);
	while(i2 < 4) {
		if(out[i_whitespace] == '	') {
			str_pntr = &out[i_whitespace];
			strcpy(str_pntr,"\0");
		}
		i2++;
		i_whitespace--;
	}
	strrev(out);
	return out;
}
char *rm_whitespace(char *str) {
	char space_char = '_'; //this is the char used for IDs when there are spaces
	size_t i_rm_whitespace = 0;
	char *dewhitespaced = GC_malloc(333);	
	while(i_rm_whitespace < strlen(str)) {
		if(str[i_rm_whitespace] != ' ') {
			dewhitespaced[i_rm_whitespace] = str[i_rm_whitespace];
		} else {
			dewhitespaced[i_rm_whitespace] = space_char;
		}
		i_rm_whitespace++;
	}
	return dewhitespaced;
}
int main(int argc,char *argv[]) {
	if(argc > 1) {
		FILE *fp;
		FILE *fp2;
		int i = 1;
		int i_link;
		int i_tags;
		int i_alphabetize;
		int i_sources;
        int l;
		int cycles;
		int dirbuftrack = 0; //directory buffer write tracker, tracks how many times directory_buffer has been added to
		int i_indentation = 0;
		int indent_level;
		long fsize = 0;
		char HEADER_BEGIN[sizeof(HEADER_BEGIN_XHTML)];
		char *fvar;
		char *fvar_buffer;
		char *str_pntr;
		char *directory_pntr;
		char filename_without_extension[GENERAL_SIZE_LIMIT];
		char filename_extension[7];
		char correct_filename_dot_whatever[argc];
		char directory_name[argc];
		char directory_buffer[GENERAL_SIZE_LIMIT][GENERAL_SIZE_LIMIT]; //directory as in directory.html
		char directory_buffer2[GENERAL_SIZE_LIMIT];
		char directory_outgoing[GENERAL_SIZE_LIMIT];
		char *fvar_buffer_sanitized = GC_malloc(0);
		char *fvar_buffer2 = GC_malloc(0);
		char *fvar_buffer3 = GC_malloc(0);
		char *fvar_buffer4 = GC_malloc(0);
		char *fvar_buffer5 = GC_malloc(0);
		char *fvar_outgoing1 = GC_malloc(GENERAL_SIZE_LIMIT);
		char *fvar_outgoing2 = GC_malloc(GENERAL_SIZE_LIMIT);
		char *fvar_outgoing3 = GC_malloc(0);
		bool title_done;
		bool content_start;
		bool content_finish;
		bool tags;
		bool tags_begun = false;
		bool review_images;
		bool review_links;
		bool review_sources;
		bool manual_directory =  false;
		bool append_directory = false;
		bool alphabetized_directory = false;
		bool body_end_found;
		bool nlp = true; //new line (equal) new paragraph (?)
		bool nlp2 = true;
		//bool style = false;
		bool more_then_one_style = false;
		bool output_type_set = false;
		bool case_begun = false;
		bool first_write = true;
		bool text_begun = true;
		bool footer_begun = true;
		bool doindent_level = true;
		bool outpute = false;
		bool template = false;
		bool run_once = false;
		GC_INIT();
		while(i < argc) {
			if(!(strstr(argv[i],"--")) && (!(strstr(argv[i],".css"))) && (fp = fopen(argv[i],"r"))) {
				if(fp != NULL) {
					fseek(fp,0,SEEK_END);
					if((fsize = ftell(fp))) {
						fseek(fp,0,SEEK_SET);
						fvar = GC_malloc(fsize++);
						fread(fvar,1,fsize,fp);
						if(strstr(argv[i],".nama")) {
							if(output_type_set == false) {
								printf("ERROR: Output not set.  Please use --xhtml or --html5.\n");
								return 0;
							}
							filename_without_extension[0] = '\0';
							correct_filename_dot_whatever[0] = '\0';
							fvar_outgoing1[0] = '\0';
							strcpy(filename_without_extension,rmsubstr(argv[i],".nama"));
							strcpy(correct_filename_dot_whatever,filename_without_extension);
							strcat(correct_filename_dot_whatever,filename_extension);
							remove(correct_filename_dot_whatever);
							fvar_buffer = strtok(fvar,"\n");
							title_done = false;
							content_finish = false;
							content_start = false;
							tags = false;
							first_write = true;
							doindent_level = false;
							cycles = 0;
							footer_begun = false;
							run_once = false;
							while(cycles == 0 || fvar_buffer != NULL) {
								if(strlen(fvar_outgoing3) < (strlen(fvar_outgoing3) + strlen(fvar_buffer))) {
									int value;
									if(strlen(fvar_outgoing3) == 0) {
										value = strlen(fvar_buffer)*300;
									} else {
										value = strlen(fvar_buffer)*strlen(fvar_outgoing3);
									}
									fvar_buffer_sanitized = GC_realloc(fvar_buffer_sanitized,value);
									fvar_buffer2 = GC_realloc(fvar_buffer2,value);
									fvar_buffer3 = GC_realloc(fvar_buffer3,value);
									fvar_buffer4 = GC_realloc(fvar_buffer4,value);
									fvar_buffer5 = GC_realloc(fvar_buffer5,value);
									strcpy(fvar_buffer2,fvar_outgoing3);
									fvar_outgoing3 = GC_realloc(fvar_outgoing3,value);
									strcpy(fvar_outgoing3,fvar_buffer2);
								}
								if(template == false) {
									strcpy(fvar_buffer_sanitized,rm_leading_whitespace(fvar_buffer));
								} else if(template == true) {
									strcpy(fvar_buffer_sanitized,fvar_buffer);
								}
								i_link = 0;
								///Below is deciding what keywords do what and converting them
								if(title_done == false && outpute == false && (strstr(fvar_buffer_sanitized,"title:"))) {
									strcat(fvar_outgoing1,HEADER_BEGIN);
									strcat(fvar_outgoing1,TITLE_BEGIN);
									strcat(fvar_outgoing1,rmsubstr(fvar_buffer_sanitized,"title: "));
									strcat(fvar_outgoing1,TITLE_END);
									title_done = true;
								}
								if(content_start == false && (strstr(fvar_buffer_sanitized,"content {"))) {
									if(outpute == false) {
										strcpy(fvar_outgoing2,HEADER_END);
										strcat(fvar_outgoing2,BODY_BEGIN);
									}
									content_start = true;
									nlp2 = false;
								}
								if(content_start == true && content_finish == false && !(strstr(fvar_buffer_sanitized,"}") && (fvar_buffer == NULL)) && !(strstr(fvar_buffer_sanitized,"tags: ")) && !(strstr(fvar_buffer_sanitized,"content {"))) {
									if(doindent_level == true) {
										doindent_level = false;
										indent_level++;
									}
									strcat(fvar_outgoing3,fvar_buffer_sanitized);
									if(strstr(fvar_buffer_sanitized,"case(")) {
										str_pntr = strstr(strstr(fvar_outgoing3,"case("),") {");
										strcpy(str_pntr,"\0");
										strcpy(fvar_buffer2,fvar_outgoing3);
										strrev(fvar_buffer2);
										str_pntr = strstr(fvar_buffer2,"(esac");
										strcpy(str_pntr,"\0");
										strrev(fvar_buffer2);
										str_pntr = strstr(fvar_outgoing3,"case(");
										strcpy(str_pntr,"\0");
										strcat(fvar_outgoing3,"<div class=\"case\">\n				<h4 id=\"");
										if((str_pntr = strstr(fvar_buffer2,")("))) {
											strcpy(fvar_buffer4,str_pntr);
											strcpy(str_pntr,"\0");
											strrev(fvar_buffer4);
											str_pntr = strstr(fvar_buffer4,"()");
											strcpy(str_pntr,"\0");
											strrev(fvar_buffer4);
											strcat(fvar_outgoing3,rm_whitespace(fvar_buffer2));
											strcat(fvar_outgoing3,"\">");
											strcat(fvar_outgoing3,a_BEGIN);
											strcat(fvar_outgoing3,fvar_buffer4);
											strcat(fvar_outgoing3,a_MIDDLE);
											strcat(fvar_outgoing3,fvar_buffer2);
											strcat(fvar_outgoing3,a_END);
											strcat(fvar_outgoing3,"</h4>");
										} else {
											strcat(fvar_outgoing3,rm_whitespace(fvar_buffer2));
											strcat(fvar_outgoing3,"\">");
											strcat(fvar_outgoing3,fvar_buffer2);
											strcat(fvar_outgoing3,"</h4>");
										}
										case_begun = true;
										nlp2 = false;
										indent_level++;
									}
									if(strstr(fvar_outgoing3,"text {")) {
										strcpy(fvar_outgoing3,"<div class=\"text-block\">");
										text_begun = true;
										nlp2 = false;
										indent_level++;
										doindent_level = true;
									}
									if(strstr(fvar_outgoing3,"sources {")) {
										strcpy(fvar_outgoing3,"<div class=\"sources\">");
										footer_begun = true;
										nlp2 = false;
										indent_level = 2;
										doindent_level = true;
									}
									if(content_start == true && content_finish == false && (strstr(fvar_outgoing3,"{img/"))) {
										fvar_buffer2[0] = '\0';
										review_images = true;
										while(review_images == true) {
											review_images = false;
											fvar_buffer2[0] = '\0';
											fvar_buffer3[0] = '\0';
											fvar_buffer4[0] = '\0';
											fvar_buffer5[0] = '\0';
											if(strstr(fvar_outgoing3,"}{")) {
												//cut out the "text"
												str_pntr = strstr(strstr(fvar_outgoing3,"{img"),"}{");
												strcpy(fvar_buffer3,str_pntr+2);
												str_pntr = strstr(fvar_buffer3,"}");
												strcpy(fvar_buffer2,str_pntr+1); //what comes after
												strcpy(str_pntr,"\0"); //fvar_buffer3 now = "text"
												//cut out the "src"
												strcpy(fvar_buffer4,fvar_outgoing3); 
												str_pntr = strstr(strstr(fvar_buffer4,"{img"),"}{");
												strcpy(str_pntr,"\0");
												strrev(fvar_buffer4);
												str_pntr = strstr(fvar_buffer4,"{");
												strcpy(str_pntr,"\0");
												strrev(fvar_buffer4); //fvar_buffer4 now = "src"
												//paste that in
												strcpy(fvar_buffer5,fvar_outgoing3);
												str_pntr = strstr(fvar_buffer5,"{img");
												strcpy(str_pntr,"\0");
												strcat(fvar_buffer5,"<img src=\"");
												strcat(fvar_buffer5,fvar_buffer4);
												strcat(fvar_buffer5,"\" alt=\"");
												strcat(fvar_buffer5,fvar_buffer3);
												strcat(fvar_buffer5,STYLE_SHEET_FINISH);
												strcat(fvar_buffer5,fvar_buffer2);
												//get the second part ready
												strcpy(fvar_outgoing3,fvar_buffer5);
											} else {
												printf("ERROR: No \"text:\" detected for \"image:\" element.\n");
												return 0;
											}
											if(strstr(fvar_outgoing3,"{img/")) {
												review_images = true;
											}
										}
									}
									if(strstr(fvar_outgoing3,"{http")) {
										fvar_buffer2[0] = '\0';
										review_links = true;
										while(review_links == true) {
											review_links = false;
											fvar_buffer2[0] = '\0';
											fvar_buffer3[0] = '\0';
											fvar_buffer4[0] = '\0';
											fvar_buffer5[0] = '\0';
											if(strstr(fvar_outgoing3,"}{")) {
												//cut out the "text"
												str_pntr = strstr(strstr(fvar_outgoing3,"{http"),"}{");
												strcpy(fvar_buffer3,str_pntr+2);
												str_pntr = strstr(fvar_buffer3,"}");
												strcpy(fvar_buffer2,str_pntr+1); //what comes after
												strcpy(str_pntr,"\0"); //fvar_buffer3 now = "text"
												//cut out the "src"
												strcpy(fvar_buffer4,fvar_outgoing3); 
												str_pntr = strstr(strstr(fvar_buffer4,"{http"),"}{");
												strcpy(str_pntr,"\0");
												strrev(fvar_buffer4);
												str_pntr = strstr(fvar_buffer4,"{");
												strcpy(str_pntr,"\0");
												strrev(fvar_buffer4); //fvar_buffer4 now = "src"
												//paste that in
												strcat(fvar_buffer5,fvar_outgoing3);
												str_pntr = strstr(fvar_buffer5,"{http");
												strcpy(str_pntr,"\0");
												strcat(fvar_buffer5,a_BEGIN);
												strcat(fvar_buffer5,fvar_buffer4);
												strcat(fvar_buffer5,a_MIDDLE);
												strcat(fvar_buffer5,fvar_buffer3);
												strcat(fvar_buffer5,a_END);
												strcat(fvar_buffer5,fvar_buffer2);
												//get the second part ready
												strcpy(fvar_outgoing3,fvar_buffer5);
											} else {
												//cut out the "src"
												strcpy(fvar_buffer4,fvar_outgoing3); 
												str_pntr = strstr(strstr(fvar_buffer4,"{http"),"}");
												strcpy(str_pntr,"\0");
												strrev(fvar_buffer4);
												str_pntr = strstr(fvar_buffer4,"{");
												strcpy(str_pntr,"\0");
												strrev(fvar_buffer4); //fvar_buffer4 now = "src"
												//paste that in
												strcat(fvar_buffer5,fvar_outgoing3);
												str_pntr = strstr(fvar_buffer5,"{http");
												strcpy(str_pntr,"\0");
												strcat(fvar_buffer5,a_BEGIN);
												strcat(fvar_buffer5,fvar_buffer4);
												strcat(fvar_buffer5,a_MIDDLE);
												strcat(fvar_buffer5,fvar_buffer4);
												strcat(fvar_buffer5,a_END);
												//get the second part ready
												strcpy(fvar_outgoing3,fvar_buffer5);
											}
											if(strstr(fvar_outgoing3,"{http")) {
												review_links = true;
											}
										}
										i_link++;
									}
									i_sources = 0;
									if(strstr(fvar_buffer_sanitized,"${") || strstr(fvar_outgoing3,"${")) {
										review_sources = true;
										while(review_sources == true) {
											fvar_buffer2[0] = '\0';
											fvar_buffer3[0] = '\0';
											fvar_buffer4[0] = '\0';
											fvar_buffer5[0] = '\0';
											review_sources = false;
											//cut out what's after ${...}
											str_pntr = strstr(strstr(fvar_outgoing3,"${"),"}");
											strcpy(fvar_buffer3,str_pntr+1); //fvar_buffer3 now = what's after ${...}
											//paste that in
											strcpy(fvar_buffer5,fvar_outgoing3);
											str_pntr = strstr(fvar_buffer5,"${");
											strcpy(str_pntr,"\0");
											//get what's inside ${...}
											strcpy(fvar_buffer4,fvar_outgoing3);
											str_pntr = strstr(strstr(fvar_buffer4,"${"),"}");
											strcpy(str_pntr,"\0");
											strrev(fvar_buffer4);
											str_pntr = strstr(fvar_buffer4,"{$");
											strcpy(str_pntr,"\0");
											strrev(fvar_buffer4); //fvar_buffer4 now = ${...}
											strcat(fvar_buffer5,sources_handler(fvar_buffer4));
											//get the second part ready
											strcpy(fvar_outgoing3,fvar_buffer5);
											strcat(fvar_outgoing3,fvar_buffer3);
											if(strstr(fvar_outgoing3,"${")) {
												review_sources = true;
											}
											i_sources++;
										}

									}
									if(strstr(fvar_outgoing3,"{footnotes}")) {
										fvar_buffer2[0] = '\0';
										fvar_buffer3[0] = '\0';
										fvar_buffer4[0] = '\0';
										fvar_buffer5[0] = '\0';
										//cut out the "text"
										str_pntr = strstr(strstr(fvar_outgoing3,"{footnotes}")+1,"}");
										strcpy(fvar_buffer3,str_pntr+1); //fvar_buffer3 now = "text"
										//paste that in
										strcpy(fvar_buffer5,fvar_outgoing3);
										str_pntr = strstr(fvar_buffer5,"{footnotes}");
										strcpy(str_pntr,"\0");
										//get the second part ready
										str_pntr = strstr(fvar_outgoing3,"{footnotes}");
										strcpy(str_pntr,fvar_buffer5);
										//strcat(fvar_outgoing3,"<div class=\"sources\">\n				        <ol>\n");
										strcat(fvar_outgoing3,"<ol>\n");
										strcat(fvar_outgoing3,src_buffer6);
										//strcat(fvar_outgoing3,"				        </ol>\n			        </div>");
										strcat(fvar_outgoing3,"				</ol>");
										strcat(fvar_outgoing3,fvar_buffer3);
										strcpy(src_buffer6,"\0");
										//printf("fvar_outgoing3 = %s\n",fvar_outgoing3);
										nlp2 = false;
										//sources = 1;
									}
									if((strstr(fvar_outgoing3,"}")) && footer_begun == true) {
										strcpy(fvar_outgoing3,"\0");
										strcat(fvar_outgoing3,"</div>");
										footer_begun = false;
										nlp2 = false;
										indent_level--;
									}
									if((strstr(fvar_outgoing3,"}")) && text_begun == true) {
										strcpy(fvar_outgoing3,"\0");
										strcat(fvar_outgoing3,"</div>");
										text_begun = false;
										nlp2 = false;
										indent_level--;
									}
									if((strstr(fvar_outgoing3,"}")) && case_begun == true) {
										strcpy(fvar_outgoing3,"\0");
										strcat(fvar_outgoing3,"</div>");
										case_begun = false;
										nlp2 = false;
										indent_level = 1;
									}
								}
								if(fvar_outgoing3[0] == '}' && content_start == true && content_finish == false && template == false) {
									if(outpute == false) {
										strcpy(fvar_outgoing3,"\n");
										strcat(fvar_outgoing3,BODY_END);
									} else if(outpute == true && template == false) {
										strcpy(fvar_outgoing3,"\0");
									}
									content_finish = true;
								}  else if(fvar_outgoing3[0] == '}' && content_start == true && content_finish == false && template == true) {
										//printf("cycles are %d fvar_outgoing3 = %s\n",cycles+1,fvar_outgoing3);
										strcpy(fvar_outgoing3,th_buffer3);
										content_finish = true;
								}
								if(title_done == true && !(content_start == true && content_finish == false) && (strstr(fvar_buffer_sanitized,"tags: ")) && (manual_directory == true || append_directory == true)) {
									if(manual_directory == true || append_directory == true) {
										strcat(directory_buffer[dirbuftrack],"                <p><a href=\"");
										strcat(directory_buffer[dirbuftrack],correct_filename_dot_whatever);
										strcat(directory_buffer[dirbuftrack],"\">");
										strcat(directory_buffer[dirbuftrack],filename_without_extension);
										strcat(directory_buffer[dirbuftrack],": ");
										strcat(directory_buffer[dirbuftrack],rmsubstr(fvar_buffer_sanitized,"tags: "));
										strcat(directory_buffer[dirbuftrack],"</a></p>\n");
									}
									tags = true;
								} else if(title_done == false && !(content_start == true && content_finish == false) && (strstr(fvar_buffer_sanitized,"tags: "))) {
									printf("ERROR: You put tags before the title, exiting.\n");
									return(0);
								} else if(cycles == 1 && (manual_directory == true || append_directory == true)) {
									printf("Directory turned ON, but no tags set!  Exiting.\n");
									return 0;
								}
								fvar_buffer = strtok(NULL,"\n"); //important
								if(fvar_buffer == NULL && content_finish == false) {
									printf("ERROR: Possible missing closing curly bracket on line %d of file %s, exiting.\n",cycles+1,argv[i]);
									remove(correct_filename_dot_whatever);
									return 0;
								}
								i_indentation = 0;
								if(strstr(fvar_buffer_sanitized,"!nlp")) {
									nlp = false;
								}
								if(template == false) {
									if(((nlp == false) || ((nlp2 == false) && (content_start == true) && (content_finish == false) && (strlen(fvar_outgoing3) > 1)))) {
										strcat(fvar_outgoing2,"\n	");
										while(i_indentation <= indent_level) {
											strcat(fvar_outgoing2,"    ");
											i_indentation++;
										}
									} else if(nlp == true && nlp2 == true && content_start == true && content_finish == false) {
										strcat(fvar_outgoing2,"\n    ");
										while(i_indentation <= indent_level) {
											strcat(fvar_outgoing2,"    ");
											i_indentation++;
										}
										strcat(fvar_outgoing2,"<p>");
										strcat(fvar_outgoing3,"</p>");
									}
								}
								nlp2 = true;
								//Below is where everything is written to files, or not
								if(title_done == true && content_start == false && fvar_buffer == NULL) {
									printf("ERROR: File %s does not have any content, ignoring.\n",argv[i]);
									remove(correct_filename_dot_whatever);

								} else if(tags == false) {
                                    if((fp2 = fopen(correct_filename_dot_whatever,"a"))) {
                                        //this is the one that writes the non-directory file
										if(first_write == true && outpute == false && template == false) {
											fprintf(fp2,"%s%s",fvar_outgoing1,single_style_handler(argc,argv));
											first_write = false;
										}
										if(outpute == false && template == false) {
											fprintf(fp2,"%s%s",fvar_outgoing2,fvar_outgoing3);
										} else if(outpute == true) {
											printf("%s\n",fvar_outgoing3);
											fprintf(fp2,"%s",fvar_outgoing3);
										} else if(template == true && first_write == true) {
											fprintf(fp2,"%s",fvar_outgoing3);
											if(run_once == false) {
												run_once = true;
												template_handler(filename_extension,filename_without_extension);
												first_write = false;
											}
										} else if(template == true && first_write == false) {
											fprintf(fp2,"\n%s",fvar_outgoing3);
										}
                                        fclose(fp2);
                                    }
									if(more_then_one_style == true) { //this might break some stuff
                                        multi_style_handler(argc,argv,fvar_outgoing1,fvar_outgoing2,fvar_outgoing3,correct_filename_dot_whatever);
                                    }
                                		tags = false;
                                        tags_begun = true;
										fvar_outgoing1[0] = '\0';
										fvar_outgoing2[0] = '\0';
										fvar_outgoing3[0] = '\0';
                                }
                                if(tags == true && manual_directory == true) { //if the tags line has been reached
                                	if(tags_begun == false) { //if this is the first run of this
	                                	remove(directory_name);
	                                    if((fp2 = fopen(directory_name,"a"))) {
		                                	fprintf(fp2,"%s%sDirectory%s%s%s\n                <h1>Directory</h1>\n",HEADER_BEGIN,TITLE_BEGIN,TITLE_END,HEADER_END,BODY_BEGIN);
		                                    fclose(fp2);
	                                    }
                                    }
                                    if(fp2 == fopen(directory_name,"a")) {
	                                	fprintf(fp2,"%s",directory_buffer[dirbuftrack]);
	                                    directory_buffer[0][0] = '\0';
	                                    fclose(fp2);
										tags = false;
									}
								}
                                    cycles++;
							}
						} else {
							printf("ERROR: File %s is not a markdown file, judging by it's extension.\n",argv[i]);
						}
					} else {
						printf("ERROR: File %s is empty, ignoring.\n",argv[i]);
					}
				} else if(fp == NULL) {
					printf("File %s is null.",argv[i]);
				}
			fclose(fp);
			//Below: Arguments are set
			} else if((strstr(argv[i],"--h") && !strstr(argv[i],"--html5") && !strstr(argv[i],"--html")) || strstr(argv[i],"--help") || strstr(argv[i],"--wtf") || strstr(argv[i],"--?")) {
				printf("Usage: namac [args] [file1] [file2] [file3] [...]\nConverts .nama markdown files, according to the syntax, to HTML5 .html or XHTML 1.0 Strict .xhtml files and lists them in a \"directory.(x)html\".\nOptions:\n   --help, --h or --?: displays this.\n   --version or --v: displays the version number.\n   --manual-directory or --manual_directory: resets to the directory.html according to the files just compiled, according to the order listed in the arguments.\n   --append-directory or --append_directory: appends to an existing directory.html.\n   --alphabetize-directory or --alphabetize_directory:  alphabetizes an already existing or commanded to exist directory.html.\n   --html5: set output file to be HTML5.\n   --xhtml: set output file to be XHTML 1.0 Strict.\n   --outpute: Print out and write without the header or <body>.\n   --template: Takes template file and puts directory where {directory} is and content where {content} is.\n");
			} else if(strstr(argv[i],"--v") || strstr(argv[i],"--ver") || strstr(argv[i],"--version") || strstr(argv[i],"--about")) {
				printf("v0.6 the nano-markdown converter (namac), 2019-2021.\n");
			} else if(strstr(argv[i],"--manual-directory") || strstr(argv[i],"--manual_directory")) {
				printf("Manual directory turned ON.\n");
				manual_directory = true;
			} else if(strstr(argv[i],"--automatic-directory") || strstr(argv[i],"--append_directory")) {
				printf("Append directory turned ON.\n");
				append_directory = true;
			} else if(strstr(argv[i],"--alphabetized-directory") || strstr(argv[i],"--alphabetized_directory") || strstr(argv[i],"--alphabetize-directory") || strstr(argv[i],"--alphabetize_directory")) {
				printf("Alphabetized directory turned ON.\n");
				alphabetized_directory = true;
			} else if((strstr(argv[i],"--style"))) {
			    printf("CSS turned ON.\n");
			    if((strstr(argv[i+2],".css"))) {
			        more_then_one_style = true;
			    }
            } else if(strstr(argv[i],"--html") || strstr(argv[i],"--html5")) {
				output_type_set = true;
				strcpy(filename_extension,".html");
				strcpy(HEADER_BEGIN,HEADER_BEGIN_HTML5);
				strcpy(directory_name,"directory");
				strcat(directory_name,filename_extension);
			} else if(strstr(argv[i],"--xhtml")) {
				strcpy(filename_extension,".xhtml");
				output_type_set = true;
				strcpy(HEADER_BEGIN,HEADER_BEGIN_XHTML);
				strcpy(directory_name,"directory");
				strcat(directory_name,filename_extension);
			} else if((strstr(argv[i],"--outpute"))) {
				outpute = true;
				strcpy(directory_name,"directory");
				strcat(directory_name,filename_extension);
			} else if(strstr(argv[i],"--template")) {
				template = true;
				
			} else if(!(strstr(argv[i],".css"))) {
				printf("ERROR: File %s does not exist, ignoring.\n",argv[i]);
			}			i++;
			if(append_directory == true && manual_directory == true) {
				printf("Haha, very funny turning on BOTH manual and append directory.  Exiting.\n");
				return 0;
			}
			if(i == argc && append_directory == true && manual_directory == false) {
				if((fp2 = fopen("directory.html","r+"))) {
					fseek(fp2,0,SEEK_END);
					fsize = ftell(fp2);
					fseek(fp2,0,SEEK_SET);
					directory_pntr = GC_malloc(fsize++);
					fread(directory_pntr,1,fsize,fp2);
					strcpy(directory_buffer2,directory_pntr);
					directory_pntr = strtok(directory_buffer2,"\n");
					fclose(fp2);
					i_tags = 0;
					body_end_found = false;
					rename(directory_name,"directory.backup");
					while(i_tags == 0 || directory_pntr != NULL) {
						directory_outgoing[0] = '\0';
						if(strstr(*directory_buffer,directory_pntr)) {
							printf("You are appending files that are already in the directory, exiting.\n");
							remove(directory_name);
							rename("directory.backup",directory_name);
							return 0;
						}
						if(strstr(directory_pntr,"</body>")) {
							body_end_found = true;
							strcpy(directory_outgoing,*directory_buffer);
							if((fp2 = fopen(directory_name,"a+"))) {
								fprintf(fp2,"%s",directory_outgoing);
								fclose(fp2);
							}
						} else if(body_end_found == false) {
							strcpy(directory_outgoing,directory_pntr);
							strcat(directory_outgoing,"\n");
							if((fp2 = fopen(directory_name,"a+"))) {
								fprintf(fp2,"%s",directory_outgoing);
								fclose(fp2);
							}
						}
						i_tags++;
						directory_pntr = strtok(NULL,"\n");
						if(directory_pntr == NULL) {
							if((fp2 = fopen(directory_name,"a+"))) {
								fprintf(fp2,"%s",BODY_END);
								fclose(fp2);
							}
						}
					}
					remove("directory.backup");
				} else {
					printf("File %s does not exist, therefore cannot append.\n",directory_name);
				}
			}
			if(tags_begun == true && argc == i && manual_directory == true /*|| append_directory == true*/) {
				if((fp2 = fopen(directory_name,"a+"))) {
					fprintf(fp2,"%s",BODY_END);
					fclose(fp2);
				}
			}
			if(i == argc && alphabetized_directory == true) {
					if((fp2 = fopen(directory_name,"r+"))) {
						fseek(fp2,0,SEEK_END);
						fsize = ftell(fp2);
						fseek(fp2,0,SEEK_SET);
						directory_pntr = GC_malloc(fsize++);
						fread(directory_pntr,1,fsize,fp2);
						strcpy(directory_buffer2,directory_pntr);
						if((directory_pntr = strstr(directory_buffer2,"</body>"))) {
							strcpy(directory_pntr,"\0");
							fclose(fp2);
							fp2 = fopen(directory_name,"w+");
							fprintf(fp2,"%s",directory_buffer2);
						}
						if((directory_pntr = strstr(directory_buffer2,"</h1>"))) {
							strrev(directory_buffer2);
							directory_pntr = strstr(directory_buffer2,">1h/<");
							strcpy(directory_pntr,"\0");
							strrev(directory_buffer2);
							fclose(fp2);
                            fp2 = fopen(directory_name,"w+");
                            fprintf(fp2,"%s",directory_buffer2);
						}
						fclose(fp2);
						fp2 = fopen(directory_name,"r+");
						i_alphabetize = 0;
						while(i_alphabetize < GENERAL_SIZE_LIMIT && fgets(directory_buffer[i_alphabetize],GENERAL_SIZE_LIMIT,fp2) != NULL) {
							i_alphabetize++;
						}
						alphabetize(directory_buffer,i_alphabetize);
						l = 0;
						if((fp2 = fopen(directory_name,"w+"))) {
							fprintf(fp2,"%s%sDirectory%s%s",HEADER_BEGIN,TITLE_BEGIN,HEADER_END,BODY_BEGIN);
							while(l < i_alphabetize) {
								if(l != 1) {
								    fprintf(fp2,"%s",directory_buffer[l]);
								}
								l++;
							}
							fprintf(fp2,"%s",BODY_END);
							fclose(fp2);
						}
					} else {
						printf("ERROR: %s does not exist, therefore cannot alphabetize, exiting.\n",directory_name);
						return 0;
					}
			}
		}
	} else {
		printf("No arguments.\n");
	}
	return 0;
}
