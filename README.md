# NAMAC, the Nano Markdown Compiler

Its main purpose is to convert "nano markdown" (a custom form of markdown) into XHTML.  It also has HTML5 support since the only real difference is how you write and the headers, although I'm not sure why you wouldn't just use XHTML seeing as it is both stricter and more well supported.

		$ namac --help
		Usage: namac [args] [file1] [file2] [file3] [...]
		Converts .nama markdown files, according to the syntax, to HTML5 .html or XHTML 1.0 Strict .xhtml files and lists them in a "directory.(x)html".
		Options:
		   --help, --h or --?: displays this.
		   --version or --v: displays the version number.
		   --manual-directory or --manual_directory: resets to the directory.html according to the files just compiled, according to the order listed in the arguments.
		   --append-directory or --append_directory: appends to an existing directory.html.
		   --alphabetize-directory or --alphabetize_directory:  alphabetizes an already existing or commanded to exist directory.html.
		   --html5: set output file to be HTML5.
		   --xhtml: set output file to be XHTML 1.0 Strict.
		   --outpute: Print out and write without the header or <body>.
		   --template: Takes template file and puts directory where {directory} is and content where {content} is.


## Quickstart

So, the most basic "namac" file would be something like this:

		title: first test
		content {
			case(hello) {
				text {
					I exist!
				}
			}
		}

You could then save this as ``test.nama`` and run ``namac --xhtml test.nama``, it will then convert it to XHTML.
To set a CSS script you could do ``namac --xhtml --style whatever.css test.nama``.

Now let's take a look at those basic components:

The 1st line, ``title: first test``, sets the ``<title>`` for the XHTML, it's rather self-explanatory.

The 2nd line ``content {`` essentially opens the body of the document, it's analogous to ``<body>`` in XHTML.

The 3rd line ``case(hello) {`` opens a "case" and sets its title line to be "hello".

The 4th line ``text {`` opens a thing inside the case where you would usually write text, it is supposed to display as a rectangle of sorts.

The 5th line ``I exist!`` is just text that you would write.  This could also be XHTML or whatever, since it would transfer over, so it would say ``<i>I exist!</i>``.  Also, it is automatically surrounded with ``<p>`` and ``</p>`` in XHTML, which can be turned off with ``!nlp`` ("!" meaning opposite or "no", new line paragraph).


Lines 6-8 are just closing curly brackets ending each element (ie ``</div>``).

##### Now let's go ahead and use the full power of namac to make something!

First, let's define a ``template`` file:

		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
				<head>
						<meta http-equiv="Content-type" content="application/xhtml+xml;charset=utf-8"/>
						{title}
						<link rel="stylesheet" href="whatever.css"/>
					</head>
				<body>
								<div class="directory_nav">
										<h3><i>Test</i></h3>
										{directory}
								</div>
								{content}
								<a href="namac/index.xhtml">Powered by NAMAC!</a>
						</body>
		</html>

This is essentially an XHTML file, but with 3 placeholders.

``{title}``, which is where the title goes.

``{directory}``, which we will define in a moment.

And finally ``{content}`` which you can guess (it's everything from the ``.nama`` file).

Second, let's define an example ``directory``:

		somefile
		howto
		stuff/index

These will all have ``.xhtml`` appended to them assuming ``--xhtml`` is specified.
Obviously, they go where ``{directory}`` goes in the template file.

Thirdly, let us define a ``webography`` file.  Webography is essentially bibliography and can be used as such:

		T: smthiread
		L: https://example.com/whatever.xhtml
		N: Something I Read
		D: 2019

		T: anthrthngiread
		L: https://example.com/sowhat.xhtml
		N: Another Thing I Read
		D: 2020

``T:`` means title (should be something quick to type), ``L:`` is link/url, ``N:`` is name (of the document/thing/source), ``D:`` is date.

Finally, our ``.nama`` file:

		title: first test
		!nlp
		content {
				case(hello)(https://example.com) {
						text {
								<b>I exist!</b>${smthiread}${anthrthngiread}
								<br/>
								I use {https://example.com}{example} as an example a lot.
								{img/someimg.ff}{A random image in the farbfeld format}
						}
						sources {
								{footnotes}
						}
				}
		}

Compile this with ``namac --xhtml --template test.nama``.

Let's go over what's new.
First off in ``case(hello)(https://example.com) {`` there is a link, which makes the title clickable.  Note: if the case title has spaces in it, the ID will have it's spaces replaced with underscores (this is for XHTML adherence).

In ``<b>I exist!</b>${smthiread}${anthrthngiread}`` the ``<b>I exist!</b>`` part is just XHTML, the ``${smthiread}${anthrthngiread}`` part references the listings inside the ``webography`` file.  They show up as [1] and [2] (``<sup><a href="#s1">[1]</a></sup><sup><a href="#s2">[2]</a></sup>`` in XHTML), and are listed in ``{footnotes}``.

The ``<br/>``, which is just straight up XHTML.

``{https://example.com}{example}`` is a link with an optional name after it, ``{https://example.com}`` would display just the link without a set name.

``{img/someimg.ff}{A random image in the farbfeld format}`` just like the link, except it's an image, as denoted be ``img/`` (so yeah, you gotta have an img dir for this to work).

``sources {`` is another element opener.

``{footnotes}`` is where references are listed, for example "1. Something I Read, 2019 https://example.com/whatever.xhtml" (``<li id="s1" value="1">Something I Read, 2019 <a href="https://example.com/whatever.xhtml">https://example.com/whatever.xhtml</a></li>`` in XHTML).



Hopefully this (ironically written in Markdown, not Nano Markdown) guide helps you in using NAMAC.  Or at least is useful to me as reference later, lol!
